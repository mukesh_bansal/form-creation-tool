<?php
use App\form;
use App\question;
use App\option;
use App\answer;
use App\sqanswer;
use App\lqanswer;
use App\seqanswer;
use App\chqanswer;
use App\raqanswer;
use App\questiontype;

/*to get the current time stamp */
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/',function(){
	/*on the homepage the display all the forms and give an option to create a new form at the top*/
	/*return view('welcome');*/

	 $forms = form::orderBy('created_at', 'asc')->get();

    return view('home', [
        'forms' => $forms
    ]);
});


/*START Form ADD/DELETE*/

Route::post('/addForm', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
    ]);

    if ($validator->fails()) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    $form = new form;
    $form->name = $request->name;
    $form->save();

    return redirect('/');

});

Route::delete('/form/{id}', function ($id) {
    form::findOrFail($id)->delete();

    return redirect('/');
});

/*END Form ADD/DELETE*/
/*START Form EDIT/
    Questions Add/DELETE*/

Route::get('/form/{name}/{id}/edit', function ($name,$id) {
    $questions = question::where('formsId', $id)->get();

    class Options{
        public $questionId;
        public $questionOptions;
    }
    $MyOptions = array();

    foreach ($questions as $question) {
        $MyOption = new Options;
        $options = option::where('questionId',$question->id)->get();
        $MyOption->questionId = $question->id;
        $MyOption->questionOptions = $options;
        $MyOptions[] = $MyOption;
    }

    $types = questiontype::orderBy('id','asc')->get();
    return view('editForm', [
        'questions' => $questions,
        'formName' => $name,
        'formId'=>$id,
        'Types' => $types,
        'options' => $MyOptions
    ]);
});

Route::post('/addQuestion/{id}', function ($id,Request $request) {
        $validator = Validator::make($request->all(), [
        'question' => 'required|max:200',
        'questionType'=>'required',
    ]);

    if ($validator->fails() || !isset($id)) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    $question = new question;
    $question->question = $request->question;
    $question->questionType = $request->questionType;
    $question->formsId = $id;
    $question->save();

    return Redirect::back();
});

Route::delete('/question/{id}', function ($id) {
    question::findOrFail($id)->delete();
    option::where('questionId',$id)->delete();

    return Redirect::back();
});

/*END Form EDIT/
    Questions Add/DELETE*/
/*START Options Adding And deleting*/
Route::post('/addOption/{id}', function ($id,Request $request) {
        $validator = Validator::make($request->all(), [
        'option' => 'required|max:200',
    ]);

    if ($validator->fails() || !isset($id)) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    $option = new option;
    $option->option = $request->option;
    $option->questionId = $id;
    $option->save();

    return Redirect::back();
});
Route::delete('/option/{id}', function ($id) {
    option::findOrFail($id)->delete();

    return Redirect::back();
});

/*End Options Adding And deleting*/

/*Start Form Results viewing in tabular form and a table to export the data in excel sheet. This view will include the answers from all the tables related to the question. Use left join so that all the questions get their answers from all the different tables*/
Route::get('/form/{name}/{id}/display', function ($name,$id) {
    
    $formQuestions = question::where('formsId', $id)->get();

    class thisFormQuestion{
        public $questionId;
        public $question;
        public $question_type;
        public $questionOptions;
    }
    $Questions = array();

    foreach ($formQuestions as $question) {
        
        $tempQuestion = new thisFormQuestion;

        $tempQuestion->questionId = $question->id;
        $tempQuestion->question = $question->question;
        $tempQuestion->question_type = $question->questionType;

        $options = option::where('questionId',$question->id)->get();
        $tempQuestion->questionOptions = $options;
        
        $Questions[] = $tempQuestion;
    }
    return view('displayform', [
        'questions' => $Questions,
        'formName' => $name,
        'formId'=>$id
    ]);
});


/*Route to store the form responses. Differentiate the user by the time of the creation of the form*/
Route::post('/submitForm/{id}', 
    function ($id,Request $request) {
        $validator = Validator::make($request->all(), 
            [        
                'question' => 'required|max:200',
                'questionType'=>'required',
            ]);
        $current_time = Carbon::now();    

        $answerJson = "{";

        $formQuestions = question::where('formsId', $id)->get();
        foreach ($formQuestions as $question) {
            if ($question->questionType == 1){
                $answerJson.='"'.$question->id.'" : "'.$request->{($question->id).'short'}.'",';
                
                $answer = new sqanswer;
                $answer->answer = $request->{($question->id).'short'};
                $answer->questionId = $question->id;
                
                $answer->created_at = $current_time;
                $answer->save();

            }
            elseif ($question->questionType == 2) {
                $answerJson.='"'.$question->id.'" : "'.$request->{($question->id).'long'}.'",';

                $answer = new lqanswer;
                $answer->answer = $request->{($question->id).'long'};
                $answer->questionId = $question->id;
                $answer->created_at = $current_time;
                $answer->save();

            }
            elseif ($question->questionType == 3) {
                
                $answerJson.='"'.$question->id.'" : "'.$request->{($question->id).'select'}.'",';

                $answer = new seqanswer;
                $answer->answer = $request->{($question->id).'select'};
                $answer->questionId = $question->id;
                $answer->created_at = $current_time;
                $answer->save();
            }
            elseif ($question->questionType == 5) {

                $answerJson.='"'.$question->id.'" : "'.$request->{($question->id).'radio'}.'",';
                $answer = new raqanswer;
                $answer->answer = $request->{($question->id).'radio'};
                $answer->questionId = $question->id;
                $answer->created_at = $current_time;
                $answer->save();

            }
            else{
                $options = option::where('questionId',$question->id)->get();
                $answerOption = "";
                foreach ($options as $option) {
                    if (isset($request->{($option->id).'check'})) {
                        /*var_dump($request->{($option->id).'check'});           */
                        $answer = new chqanswer;

                        $answerOption.= $request->{($option->id).'check'}.",";

                        $answer->answer = $request->{($option->id).'check'};
                        $answer->questionId = $question->id;
                        $answer->created_at = $current_time;
                        $answer->save();
                    }
                }
                $answerJson.='"'.$question->id.'" : "'.$answerOption.'",';

            }
        }
        $answerJson = substr($answerJson, 0, -1);
        $answerJson.= "}";

        $Json = json_encode($answerJson);

        $answer = new answer;
        $answer->answer = $Json;
        $answer->formid = $id;
        $answer->save();
        return Redirect('/');
});


/*Exporting form results in an excel file*/
Route::get('/form/{name}/{id}/export', function ($name,$id) {
    
    $formQuestions = question::where('formsId', $id)->get();

    class thisFormQuestion{
        public $questionId;
        public $question;
        public $question_type;
        public $questionOptions;
    }
    $Questions = array();

    foreach ($formQuestions as $question) {
        
        $tempQuestion = new thisFormQuestion;

        $tempQuestion->questionId = $question->id;
        $tempQuestion->question = $question->question;
        $tempQuestion->question_type = $question->questionType;

        $options = option::where('questionId',$question->id)->get();
        $tempQuestion->questionOptions = $options;
        
        $Questions[] = $tempQuestion;
    }
    $formAnswers = answer::where('formid',$id)->get();
    $formAnswersArray = array();
    foreach ($formAnswers as $answer) {
        $results = json_decode($answer->answer,true);
        $results = json_decode($results,true);
        $answerArray = array();
        foreach ($results as $id => $answer) {
            /*var_dump($id .'->'. $answer);*/
            $answerArray[] = $answer;
        }
        /*  var_dump($answerArray);*/
        $formAnswersArray[] = $answerArray;
    }
    Excel::create($name,function($excel) use ($formAnswersArray,$name){
        $excel->setTitle($name);
        $excel->setCreator('Mickey')->setCompany('Ahmedabad Global Shapers');
        $excel->setDescription($name." Answers");

        $excel->sheet('sheet1',function ($sheet) use ($formAnswersArray){
            $sheet->fromArray($formAnswersArray,null,'A1',false,false);
        });
    })->download('xlsx');

});
