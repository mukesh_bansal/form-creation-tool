@extends('includes.master')

<!-- the title of the page can be included in this section  -->
@section('title')
Editing {{$formName}}
@endsection

<!-- This section will be replaced at the position of the content in the master layout -->
@section('content')

{{-- script to add and delete options --}}
<script type="text/javascript">
    


</script>

<div class="panel-body">
<!-- Display Validation Errors -->
@include('errors.errors')

<!-- Current Questions And Their Type -->
@if (count($questions) > 0)
    <div class="panel panel-default">
        <div class="panel-heading">
            {{$formName}}
        </div>

        <div class="panel-body">
            <table class="table table-striped task-table">

                <!-- Table Headings -->
                <thead>
                    <th>Question</th>
                    <th>Type</th>
                    <th>Options</th>
                    <th>&nbsp;</th>
                </thead>

                <!-- Table Body -->
                <tbody>
                    @foreach ($questions as $question)
                        <tr>
                            <!-- Question Name -->
                            <td class="table-text">
                                    <div>{{ $question->question }}</div>
                            </td>
                            <td class="table-text">
                                    <div>
                                        @foreach($Types as $type)
                                            @if($type->id == $question->questionType)
                                            {{ $type->questionType }}
                                            @endif
                                        @endforeach
                                    </div>
                            </td>
                            <td class="table-text" id="options">
                            @if($question->questionType>2)
                            {{-- options is an object where all the details of the question types is stored and the details of question id and the options list is stored as an object itself --}}
                                @foreach($options as $option)
                                    @if($option->questionId == $question->id)
                                        @foreach($option->questionOptions as $questionOption)
                                        <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                {{ $questionOption->option}}
                                            </div>
                                            <div class="col-sm-3">
                                                <form name="deleteOption{{ $questionOption->id }}" action="/option/{{ $questionOption->id }}" method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <a href="#" onclick="document.forms['deleteOption{{ $questionOption->id }}'].submit();"> <i class="fa fa-remove" style="color: red"></i>
                                                    </a>                                               
                                                    <input type="hidden" name="_method" value="DELETE">
                                                </form>
                                            </div>
                                        </div>
                                        </div>
                                        <br>
                                        @endforeach
                                    @endif
                                @endforeach
                                <!-- New Option -->
                                <form action="/addOption/{{$question->id}}" method="POST" class="form-horizontal">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <input type="text" name="option" id="option" class="form-control">
                                        </div>
                                        <div class="col-sm-3">
                                            <button type="submit" class="btn btn-success">
                                                <i class="fa fa-plus"></i> Add
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                @endif
                            </td>

                            <td>
                                <!-- TODO: Delete Button -->
                                <form action="/question/{{ $question->id }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button class="btn btn-danger">Delete Question</button>
                                    <input type="hidden" name="_method" value="DELETE">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif

<!-- New Question -->
<form action="/addQuestion/{{$formId}}" method="POST" class="form-horizontal">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="form" class="col-sm-3 control-label">Question Type</label>

        <div class="col-sm-6">
            <select name="questionType" id="Form-questionType" class="form-control">
                @foreach($Types as $Type)
                    <option value="{{$Type->id}}">{{$Type->questionType}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="form" class="col-sm-3 control-label">Question</label>

        <div class="col-sm-6">
            <input type="text" name="question" id="Form-question" class="form-control">
        </div>
    </div>

    <!-- Add Question Button -->
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <button type="submit" class="btn btn-success">
                <i class="fa fa-plus"></i> Add Question
            </button>
        </div>
    </div>
</form>
</div>

@endsection