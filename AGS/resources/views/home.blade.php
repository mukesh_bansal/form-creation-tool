@extends('includes.master')

<!-- the title of the page can be included in this section  -->
@section('title')
Dynamic Form Creation
@endsection

<!-- This section will be replaced at the position of the content in the master layout -->
@section('content')

<div class="panel-body">
<!-- Display Validation Errors -->
@include('errors.errors')

<!-- Current Forms -->
@if (count($forms) > 0)
    <div class="panel panel-default">
        <div class="panel-heading">
            Current Forms
        </div>

        <div class="panel-body">
            <table class="table table-striped task-table">

                <!-- Table Headings -->
                <thead>
                    <th>Form</th>
                    <th>&nbsp;</th>
                </thead>

                <!-- Table Body -->
                <tbody>
                    @foreach ($forms as $form)
                        <tr>
                            <!-- Form Name -->
                            <td class="table-text">
                                <a href="/form/{{$form->name}}/{{$form->id}}/edit">
                                    <div>{{ $form->name }}</div>
                                </a>
                            </td>
                            <td class="table-text pull-right">
                                <!-- TODO: Delete Button -->
                                <form action="/form/{{ $form->id }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button class="btn btn-danger">Delete Form</button>
                                    <input type="hidden" name="_method" value="DELETE">
                                </form>
                            </td>

                            <td class="table-text">
                                <a href="/form/{{$form->name}}/{{$form->id}}/display">
                                    <button class="btn btn-primary">Display Form</button>
                                </a>
                            </td>

                            <td class="table-text">
                                <a href="/form/{{$form->name}}/{{$form->id}}/export">
                                    <button class="btn btn-default">Export Results</button>
                                </a>
                            </td>
                            
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif

<!-- New Form Form -->
<form action="/addForm" method="POST" class="form-horizontal">
    {{ csrf_field() }}

    <!-- Form Name -->
    <div class="form-group">
        <label for="form" class="col-sm-3 control-label">Form Name</label>

        <div class="col-sm-6">
            <input type="text" name="name" id="Form-name" class="form-control">
        </div>
    </div>

    <!-- Add Form Button -->
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <button type="submit" class="btn btn-success">
                <i class="fa fa-plus"></i> Add Form
            </button>
        </div>
    </div>
</form>
</div>

@endsection