@extends('includes.master')

<!-- the title of the page can be included in this section  -->
@section('title')
{{$formName}}
@endsection

<!-- This section will be replaced at the position of the content in the master layout -->
@section('content')

{{-- script to add and delete options --}}
<script type="text/javascript">
    


</script>

<div class="panel-body text-center">
<div class="container col-md-offset-3 col-md-6 text-center">
    <!-- Display Validation Errors -->
    @include('errors.errors')
    <!-- Form Questions -->
    <form action="/submitForm/{{$formId}}" method="post" class="form-horizontal">
        {{ csrf_field() }}
        @foreach($questions as $question)

        <div class="form-group">
            <div class="row">
                <h4><label class="pull-left" for="usr">{{$question->question}}</label></h4>
            </div>
            @if($question->question_type == 1)
                <input required type="text" name="{{$question->questionId}}short" class="form-control" placeholder="Your Answer">
            @elseif($question->question_type == 2)
                <textarea required name="{{$question->questionId}}long" class="form-control" rows="3"></textarea>
            @elseif($question->question_type == 3)
                <select required class="form-control" name="{{$question->questionId}}select">
                    @foreach($question->questionOptions as $option)
                        <option value="{{$option->option}}">{{$option->option}}</option>
                    @endforeach
                </select>
            @elseif($question->question_type == 4)
                @foreach($question->questionOptions as $option)
                        <input type="checkbox" name="{{$option->id}}check" id="{{$option->option}}" value="{{$option->option}}"> {{$option->option}}
                @endforeach
            @elseif($question->question_type == 5)
                @foreach($question->questionOptions as $option)
                <div class="row">
                    
                <div class="radio pull-left">
                  <label><input required type="radio" name="{{$question->questionId}}radio" id="{{$option->option}}" value="{{$option->option}}"> {{$option->option}}</label>
                </div>
                </div>
                @endforeach
            @endif
        </div>
        @endforeach
        <button type="submit" class="btn btn-lg btn-success"> Submit</button>
    </form>
</div>
</div>

@endsection