<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSQAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SQAnswers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('answer','200');
            $table->integer('questionId');
            $table->timestamps();
            $table->foreign('questionId')->references('id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SQAnswers');
    }
}
