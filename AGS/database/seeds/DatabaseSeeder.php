<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('questionTypes')->insert(['questionType' => 'SQ']);
        DB::table('questionTypes')->insert(['questionType' => 'LQ']);
        DB::table('questionTypes')->insert(['questionType' => 'SeQ']);
        DB::table('questionTypes')->insert(['questionType' => 'ChQ']);
        DB::table('questionTypes')->insert(['questionType' => 'RaQ']);
    }
}
